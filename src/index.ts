import { ExpressRouter } from '@kominal/lib-node-express-router';
import { MongoDBInterface } from '@kominal/lib-node-mongodb-interface';
import { info, startObserver, warn } from '@kominal/observer-node-client';
import { ChannelWrapper, connect } from 'amqp-connection-manager';
import { ConfirmChannel } from 'amqplib';
import { AsyncClient, connect as connectMqtt } from 'async-mqtt';
import { OBSERVER_MQTT_BROKER, OBSERVER_RABBITMQ_BROKER, SERVICE_TOKEN } from './helper/environment';
import { onEvent, onLog } from './ingress';
import { Event, EventDatabase } from './models/event';
import { Log, LogDatabase } from './models/log';
import { emqxRouter } from './routes/emqx';
import { eventRouter } from './routes/event';
import { logRouter } from './routes/log';
import { masterDataRouter } from './routes/master-data';
import { tenantsRouter } from './routes/tenants';
import { CacheScheduler, masterDataCache } from './scheduler/cache.scheduler';
import { SwarmScheduler } from './scheduler/swarm.scheduler';
import { UptimeScheduler } from './scheduler/uptime.scheduler';

let mongoDBInterface: MongoDBInterface;
let expressRouter: ExpressRouter;
export let rmqClient: ChannelWrapper | undefined;
export let mqttClient: AsyncClient | undefined;

async function start() {
	startObserver();
	mongoDBInterface = new MongoDBInterface('controller');
	await mongoDBInterface.connect();
	rmqClient = connect(OBSERVER_RABBITMQ_BROKER.split(',')).createChannel({
		json: true,
		setup: async (channel: ConfirmChannel) => {
			await channel.assertQueue('observer.log.inbound', { durable: false, messageTtl: 600000 });
			await channel.assertQueue('observer.event.inbound', { durable: false, messageTtl: 600000 });
			channel.consume('observer.log.inbound', (msg) => onLog(channel, msg));
			channel.consume('observer.event.inbound', (msg) => onEvent(channel, msg));
		},
	});

	await new CacheScheduler().start(60);

	expressRouter = new ExpressRouter({
		baseUrl: 'controller',
		healthCheck: async () => true,
		routes: [emqxRouter, logRouter, eventRouter, masterDataRouter, tenantsRouter],
	});
	await expressRouter.start();

	mqttClient = connectMqtt(OBSERVER_MQTT_BROKER, {
		username: 'SERVICE',
		password: SERVICE_TOKEN,
	});
	mqttClient.on('message', async (topic, buffer) => {
		const parts = topic.split('/');
		if (parts.length != 6) {
			return;
		}
		const [tenantId, projectName, environmentName, serviceName, taskId, messageType] = parts;

		if (!masterDataCache.masterData[tenantId]) {
			return;
		}

		const body = JSON.parse(buffer.toString('utf-8'));

		if (messageType === 'event') {
			const { time, type, content }: Event = body;
			await EventDatabase.create({
				tenantId,
				projectName,
				environmentName,
				serviceName,
				taskId,
				time,
				type,
				content,
			});
		} else if (messageType === 'log') {
			const { time, level, message }: Log = body;
			await LogDatabase.create({
				tenantId,
				projectName,
				environmentName,
				serviceName,
				taskId,
				time,
				level,
				message,
			});
		} else {
			warn(`Received invalid message: ${JSON.stringify(body)}`);
		}
	});
	mqttClient.on('error', (e) => {
		console.log(e);
	});
	mqttClient;
	await mqttClient.subscribe('#');

	const swarmScheduler = new SwarmScheduler();
	await swarmScheduler.start(1800);
	await new UptimeScheduler(swarmScheduler).start(300);
}
start();

process.on('SIGTERM', async () => {
	info(`Received system signal 'SIGTERM'. Shutting down service...`);
	expressRouter.getServer()?.close();
	await mongoDBInterface.disconnect();
});
