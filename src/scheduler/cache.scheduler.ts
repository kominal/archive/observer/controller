import { Scheduler } from '@kominal/lib-node-scheduler';
import { EventDatabase } from '../models/event';
import { LogDatabase } from '../models/log';
import { MasterData, MasterDataCache } from '../models/master-data-cache';

export let masterDataCache: MasterDataCache = { valid: false, masterData: {} };

type AggregateResult = {
	tenantId: string;
	projectName: string;
	environmentName: string;
	serviceName: string;
}[];

export class CacheScheduler extends Scheduler {
	constructor() {
		super();
	}

	async run(): Promise<void> {
		if (masterDataCache.valid) {
			return;
		}

		const masterData: MasterData = {};

		const eventMasterData: AggregateResult = await EventDatabase.aggregate([
			{
				$group: {
					_id: { tenantId: '$tenantId', projectName: '$projectName', environmentName: '$environmentName', serviceName: '$serviceName' },
				},
			},
			{
				$project: {
					_id: 0,
					tenantId: '$_id.tenantId',
					projectName: '$_id.projectName',
					environmentName: '$_id.environmentName',
					serviceName: '$_id.serviceName',
				},
			},
		]);
		addToMasterData(masterData, eventMasterData);

		const logMasterData: AggregateResult = await LogDatabase.aggregate([
			{
				$group: {
					_id: { tenantId: '$tenantId', projectName: '$projectName', environmentName: '$environmentName', serviceName: '$serviceName' },
				},
			},
			{
				$project: {
					_id: 0,
					tenantId: '$_id.tenantId',
					projectName: '$_id.projectName',
					environmentName: '$_id.environmentName',
					serviceName: '$_id.serviceName',
				},
			},
		]);
		addToMasterData(masterData, logMasterData);

		masterDataCache.masterData = masterData;
		masterDataCache.valid = true;
	}
}

function addToMasterData(masterData: MasterData, results: AggregateResult) {
	for (const { tenantId, projectName, environmentName, serviceName } of results) {
		if (!masterData[tenantId]) {
			masterData[tenantId] = {};
		}

		if (!masterData[tenantId][projectName]) {
			masterData[tenantId][projectName] = {};
		}

		if (!masterData[tenantId][projectName][environmentName]) {
			masterData[tenantId][projectName][environmentName] = [];
		}

		if (!masterData[tenantId][projectName][environmentName].includes(serviceName)) {
			masterData[tenantId][projectName][environmentName].push(serviceName);
		}
	}
}
