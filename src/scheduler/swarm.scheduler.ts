import { Scheduler } from '@kominal/lib-node-scheduler';
import { error } from '@kominal/observer-node-client';
import Docker from 'dockerode';
import { UptimeCheck } from '../models/uptime-check';

export class SwarmScheduler extends Scheduler {
	private docker = new Docker();

	public uptimeChecks: UptimeCheck[] = [];

	async run(): Promise<void> {
		const updatedUptimeChecks: UptimeCheck[] = [];

		const services = await this.docker.listServices();
		for (const service of services) {
			const serviceObject = this.docker.getService(service.ID);
			const inspect = await serviceObject.inspect();
			const labels = inspect.Spec.Labels;
			for (const label in labels) {
				if (label.startsWith('kominal.observer')) {
					try {
						const parts = label.split('.').slice(2);
						const [projectName, environmentName, serviceName, type] = parts;
						if (type === 'uptime') {
							let check = updatedUptimeChecks.find(
								(c) => c.projectName === projectName && c.environmentName === environmentName && c.serviceName === serviceName
							);

							if (!check) {
								check = { projectName, environmentName, serviceName, url: '' };
								updatedUptimeChecks.push(check);
							}
							check.url = labels[label];
						}
					} catch (e) {
						error(e);
					}
				}
			}
		}

		this.uptimeChecks = updatedUptimeChecks;
	}
}
