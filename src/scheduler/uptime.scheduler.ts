import { Scheduler } from '@kominal/lib-node-scheduler';
import { error } from '@kominal/observer-node-client';
import Axios from 'axios';
import { EventDatabase } from '../models/event';
import { SwarmScheduler } from './swarm.scheduler';

const tenantId = '601d7bc0c0e774e81a58008f';
export class UptimeScheduler extends Scheduler {
	constructor(private swarmScheduler: SwarmScheduler) {
		super();
	}

	async run(): Promise<void> {
		for (const uptimeCheck of this.swarmScheduler.uptimeChecks) {
			let type = 'SERVICE_DOWN';
			const start = Date.now();
			try {
				const { status } = await Axios.get(uptimeCheck.url);
				if (status === 200) {
					type = 'SERVICE_UP';
				}
			} catch (e) {
				error(`Uptime check failed: ${JSON.stringify(uptimeCheck)}`);
			}
			await EventDatabase.create({
				tenantId,
				...uptimeCheck,
				taskId: undefined,
				time: new Date(),
				type,
				content: { duration: Date.now() - start },
			});
		}
	}
}
