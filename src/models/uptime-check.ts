export interface UptimeCheck {
	projectName: string;
	environmentName: string;
	serviceName: string;
	url: string;
}
