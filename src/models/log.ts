import { Document, model, Schema } from '@kominal/lib-node-mongodb-interface';

export interface Log {
	_id?: string;
	tenantId: string;
	projectName: string;
	environmentName: string;
	serviceName: string;
	taskId: string;
	time: Date;
	level: string;
	message: any;
}

export const LogDatabase = model<Document & Log>(
	'Log',
	new Schema(
		{
			tenantId: { type: Schema.Types.ObjectId, ref: 'Tenant' },
			projectName: String,
			environmentName: String,
			serviceName: String,
			taskId: String,
			time: Date,
			level: String,
			message: Schema.Types.Mixed,
		},
		{ minimize: false }
	)
		.index({ time: 1 }, { expireAfterSeconds: 20160 })
		.index({ time: 1, tenantId: 1, projectName: 1, environmentName: 1, serviceName: 1, level: 1, taskId: 1 })
);
