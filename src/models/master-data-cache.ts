export interface TenantMasterData {
	[projectName: string]: {
		[environmentName: string]: string[];
	};
}

export interface MasterData {
	[tenantId: string]: TenantMasterData;
}

export interface MasterDataCache {
	valid: boolean;
	masterData: MasterData;
}
