import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse } from '@kominal/lib-node-mongodb-interface';
import { createTimeFilter } from '../helper/helper';
import { Log, LogDatabase } from '../models/log';

export const logRouter = new Router();

logRouter.getAsUser<
	{ projectName?: string; environmentName?: string; serviceName?: string },
	PaginationResponse<Log>,
	PaginationRequest & { start?: string; end?: string; maxId?: string }
>('/logs/:projectName?/:environmentName?/:serviceName?', async (req) => {
	const idFilter = req.query.maxId ? { _id: { $lt: req.query.maxId } } : {};
	const responseBody = await applyPagination<Log>(LogDatabase, req.query, {
		filter: {
			projectName: req.params.projectName,
			environmentName: req.params.environmentName,
			serviceName: req.params.serviceName,
			...createTimeFilter(req.query),
			...idFilter,
		},
		sorting: { time: -1 },
		count: false,
	});
	return { statusCode: 200, responseBody };
});
