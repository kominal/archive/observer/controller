import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse } from '@kominal/lib-node-mongodb-interface';
import { get } from 'object-path';
import { createTimeFilter } from '../helper/helper';
import { Event, EventDatabase } from '../models/event';

export const eventRouter = new Router();

eventRouter.getAsUser<
	{ projectName?: string; environmentName?: string; serviceName?: string },
	{ name: string; series: { name: string; value: number }[] }[],
	{ start?: string; end?: string; type?: string; aggregation: 'count' | 'min' | 'max' | 'avg' | 'sum' | 'value'; field?: string }
>('/events/aggregate/:projectName?/:environmentName?/:serviceName?', async (req, res) => {
	const aggregations: any[] = [];

	aggregations.push({
		$match: {
			projectName: req.params.projectName,
			environmentName: req.params.environmentName,
			serviceName: req.params.serviceName,
			//...createTimeFilter(req.query),
			type: req.query.type,
		},
	});

	if (req.query.aggregation === 'count') {
		aggregations.push({ $count: 'result' });
	} else if (req.query.aggregation === 'min' && req.query.field) {
		aggregations.push({ $group: { _id: null, result: { $min: `$${req.query.field}'` } } });
	} else if (req.query.aggregation === 'max' && req.query.field) {
		aggregations.push({ $group: { _id: null, result: { $max: `$${req.query.field}'` } } });
	} else if (req.query.aggregation === 'avg' && req.query.field) {
		aggregations.push({ $group: { _id: null, result: { $avg: `$${req.query.field}'` } } });
	} else if (req.query.aggregation === 'sum' && req.query.field) {
		aggregations.push({ $group: { _id: null, result: { $sum: `$${req.query.field}'` } } });
	} else if (req.query.aggregation === 'value' && req.query.field) {
	} else {
		throw new Error('transl.error.parameter.missing');
	}

	const aggregationResult = await EventDatabase.aggregate<{ result: number }>(aggregations).exec();

	const responseBody: { name: string; series: { name: string; value: number }[] }[] = [];

	if (req.query.aggregation === 'value' && req.query.field) {
		for (const event of aggregationResult as Event[]) {
			let line = responseBody.find((s) => s.name === event.type);
			if (!line) {
				line = { name: event.type, series: [] };
				responseBody.push(line);
			}

			line.series.push({
				name: event.time.toString(),
				value: get(event, req.query.field),
			});
		}
	}

	return { statusCode: 200, responseBody };
});

eventRouter.getAsUser<
	{ projectName?: string; environmentName?: string; serviceName?: string },
	PaginationResponse<Event>,
	PaginationRequest & { start?: string; end?: string; maxId?: string }
>('/events/:projectName?/:environmentName?/:serviceName?', async (req) => {
	const idFilter = req.query.maxId ? { _id: { $lt: req.query.maxId } } : {};
	const responseBody = await applyPagination<Event>(EventDatabase, req.query, {
		filter: {
			projectName: req.params.projectName,
			environmentName: req.params.environmentName,
			serviceName: req.params.serviceName,
			...createTimeFilter(req.query),
			...idFilter,
		},
		sorting: { time: -1 },
		count: false,
	});
	return { statusCode: 200, responseBody };
});
