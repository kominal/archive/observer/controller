import { Router } from '@kominal/lib-node-express-router';
import { error } from '@kominal/observer-node-client';
import Axios from 'axios';
import { CORE_BASE_URL, CORE_SERVICE_TOKEN } from '../helper/environment';
import { EventDatabase } from '../models/event';
import { LogDatabase } from '../models/log';
import { Tenant, TenantDatabase } from '../models/tenant';

export const tenantsRouter = new Router();

tenantsRouter.getAsUser<never, Tenant[], never>('/tenants', async (req, res, userId, payload) => {
	if (!payload) {
		throw new Error('transl.error.missing.payload');
	}

	const responseBody = await TenantDatabase.find({ _id: { $in: Object.keys(payload.tenants) } }).lean<Tenant>();

	return {
		statusCode: 200,
		responseBody,
	};
});

tenantsRouter.getAsUser<{ tenantId: string }, Tenant, never>('/tenants/:tenantId', async (req) => {
	return {
		statusCode: 200,
		responseBody: await TenantDatabase.findOne({ _id: req.params.tenantId }).orFail(new Error('transl.error.notFound.category')),
	};
});

tenantsRouter.postAsUser<never, Tenant, { _id: string }, never>('/tenants', async (req, res, userId) => {
	delete req.body._id;

	const tenant = await TenantDatabase.create(req.body);

	try {
		await Axios.put(
			`https://${CORE_BASE_URL}/user-service/user/${userId}/permissions?tenantId=${tenant._id}`,
			{
				permissions: ['core.user-service.write', 'papyrus.controller.tenant.settings'],
			},
			{
				headers: {
					Authorization: CORE_SERVICE_TOKEN,
					userId,
				},
			}
		);
	} catch (e) {
		await tenant.remove();
		error(`Could not set user permissions: ${JSON.stringify(e?.response?.data)}`);
		throw new Error('transl.error.tenantCreationFailed');
	}

	return {
		statusCode: 200,
		responseBody: {
			_id: tenant._id,
		},
	};
});

tenantsRouter.putAsUser<{ tenantId: string }, Tenant & { tenantId: never }, never, never>(
	'/tenants/:tenantId',
	async (req) => {
		delete req.body.tenantId;
		await TenantDatabase.updateMany({ _id: req.params.tenantId }, req.body);
		return {
			statusCode: 200,
		};
	},
	{ permissions: ['papyrus.controller.tenant.settings'] }
);

tenantsRouter.deleteAsUser<{ tenantId: string }, never, never>('/tenants/:tenantId', async (req, res, userId) => {
	const tenant = await TenantDatabase.findById(req.params.tenantId).orFail(new Error('transl.error.notFound.tenant'));

	try {
		await Axios.delete(`https://${CORE_BASE_URL}/user-service/tenants/${tenant._id}`, {
			headers: {
				Authorization: CORE_SERVICE_TOKEN,
			},
		});

		await EventDatabase.deleteMany({ tenantId: tenant._id });
		await LogDatabase.deleteMany({ tenantId: tenant._id });
		await tenant.remove();
	} catch (e) {
		error(`Could not delete tenant in user service: ${JSON.stringify(e?.response?.data)}`);
		throw new Error('transl.error.tenantDeletionFailed');
	}

	return {
		statusCode: 200,
	};
});
