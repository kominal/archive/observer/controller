import { Router } from '@kominal/lib-node-express-router';
import { TenantMasterData } from '../models/master-data-cache';
import { masterDataCache } from '../scheduler/cache.scheduler';

export const masterDataRouter = new Router();

masterDataRouter.getAsUser<{ tenantId: string }, TenantMasterData, never>('/tenants/:tenantId/masterData', async (req) => {
	return { statusCode: 200, responseBody: masterDataCache.masterData[req.params.tenantId] || {} };
});
