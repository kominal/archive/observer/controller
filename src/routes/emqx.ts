import { Router } from '@kominal/lib-node-express-router';
import { SERVICE_TOKEN } from '../helper/environment';

export const emqxRouter = new Router();

emqxRouter.getAsGuest<never, never, never>('/emqx/auth', async (req) => {
	console.log('Auth', req.body, req.params, req.query);
	return { statusCode: 200 };
});

emqxRouter.getAsGuest<never, never, { username: string; password: string }>('/emqx/service', async (req) => {
	const { username, password } = req.query;
	if (username === 'SERVICE' && password === SERVICE_TOKEN) {
		return { statusCode: 200 };
	}
	return { statusCode: 401 };
});

emqxRouter.getAsGuest<never, never, never>('/emqx/acl', async (req) => {
	console.log('ACL', req.body, req.params, req.query);
	return { statusCode: 200 };
});
