import { warn } from '@kominal/observer-node-client';
import { ConfirmChannel, ConsumeMessage } from 'amqplib';
import { Event, EventDatabase } from './models/event';
import { Log, LogDatabase } from './models/log';
import { masterDataCache } from './scheduler/cache.scheduler';

const tenantId = '601d7bc0c0e774e81a58008f';

export async function onLog(channel: ConfirmChannel, msg: ConsumeMessage | null): Promise<void> {
	if (!msg) {
		warn('Received log without content.');
		return;
	}

	const log: Log = JSON.parse(msg.content.toString());
	await LogDatabase.create({ ...log, tenantId });

	if (
		!masterDataCache.masterData[tenantId] ||
		!masterDataCache.masterData[tenantId][log.projectName] ||
		!masterDataCache.masterData[tenantId][log.projectName][log.environmentName] ||
		!masterDataCache.masterData[tenantId][log.projectName][log.environmentName].includes(log.serviceName)
	) {
		masterDataCache.valid = false;
	}

	channel.ack(msg);
}

export async function onEvent(channel: ConfirmChannel, msg: ConsumeMessage | null): Promise<void> {
	if (!msg) {
		warn('Received event without content.');
		return;
	}

	const event: Event = JSON.parse(msg.content.toString());
	await EventDatabase.create({ ...event, tenantId });

	if (
		!masterDataCache.masterData[tenantId] ||
		!masterDataCache.masterData[tenantId][event.projectName] ||
		!masterDataCache.masterData[tenantId][event.projectName][event.environmentName] ||
		!masterDataCache.masterData[tenantId][event.projectName][event.environmentName].includes(event.serviceName)
	) {
		masterDataCache.valid = false;
	}

	channel.ack(msg);
}
