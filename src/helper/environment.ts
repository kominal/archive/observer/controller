import { getEnvironmentString } from '@kominal/lib-node-environment';

export const OBSERVER_RABBITMQ_BROKER = getEnvironmentString('OBSERVER_RABBITMQ_BROKER');
export const OBSERVER_MQTT_BROKER = getEnvironmentString('OBSERVER_MQTT_BROKER');
export const CORE_BASE_URL = getEnvironmentString('CORE_BASE_URL', 'core.kominal.app');
export const CORE_SERVICE_TOKEN = getEnvironmentString('CORE_SERVICE_TOKEN');
export const SERVICE_TOKEN = getEnvironmentString('SERVICE_TOKEN');
