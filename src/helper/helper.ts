export function createTimeFilter(query: { start?: string; end?: string }): Promise<any> {
	let filter: any = {};
	if (query.start) {
		filter.time = filter.time || {};
		filter.time.$gt = new Date(query.start);
	}
	if (query.end) {
		filter.time = filter.time || {};
		filter.time.$lt = new Date(query.end);
	}
	return filter;
}
